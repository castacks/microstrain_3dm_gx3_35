/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include "ros/ros.h"
#include "sensor_msgs/Imu.h"
#include <microstrain_3dm_gx3_35/ImuExtra.h>
#include <microstrain_3dm_gx3_35/ImuExtra.h>
#include <queue>

class MicrostrainRestamper {

public:
  MicrostrainRestamper(ros::NodeHandle nh, ros::NodeHandle np);
  ~MicrostrainRestamper();
  void imuCallback(const sensor_msgs::Imu::ConstPtr& msg);
  void imuExtraCallback(const microstrain_3dm_gx3_35::ImuExtra::ConstPtr& msg);
  void processQueue( void );

private:
  
  ros::NodeHandle nh_, np_;
  ros::Publisher imu_pub_;
  ros::Subscriber imu_sub_;
  ros::Subscriber imu_extra_sub_;

  std::queue<sensor_msgs::Imu> imuMsgs;
  std::queue<microstrain_3dm_gx3_35::ImuExtra> extraMsgs;
  

};
 
  // Constructor 
  MicrostrainRestamper::MicrostrainRestamper(ros::NodeHandle nh, ros::NodeHandle np):
    nh_(nh),
    np_(np)
  {
    imu_sub_ = nh_.subscribe("/microstrain/imu", 100, &MicrostrainRestamper::imuCallback, this);
    imu_extra_sub_ = nh_.subscribe("/microstrain/imu_extra", 100, &MicrostrainRestamper::imuExtraCallback, this);

    imu_pub_ = nh_.advertise<sensor_msgs::Imu>("/microstrain/imu_restamped", 100);
    
    //imuMsgs.clear();
    //extraMsgs.clear();
  }
  
  MicrostrainRestamper::~MicrostrainRestamper()
  {
    //imuMsgs.clear();
    //extraMsgs.clear();
  }


  void MicrostrainRestamper::imuCallback(const sensor_msgs::Imu::ConstPtr& msg)
  {
    imuMsgs.push(*msg); 
    processQueue();

  } 

  void MicrostrainRestamper::imuExtraCallback(const microstrain_3dm_gx3_35::ImuExtra::ConstPtr& msg)
  {
    extraMsgs.push(*msg);
    processQueue();
  }
  
  void MicrostrainRestamper::processQueue( void )
  {
    while (!imuMsgs.empty() && !extraMsgs.empty())
    {
      if (imuMsgs.front().header.seq == extraMsgs.front().header.seq)
      {
        // Create new imu message with correct stamp and publish it
        
        sensor_msgs::Imu restampedMsg = imuMsgs.front();
        restampedMsg.header = extraMsgs.front().header;
        // It seems that you cannot directly modify the seq number
        //restampedMsg.header.seq = extraMsgs.front().header.seq;
        imu_pub_.publish(restampedMsg);
        
        // Discard the used messages
        extraMsgs.pop();
        imuMsgs.pop();
      }
      else if (imuMsgs.front().header.seq > extraMsgs.front().header.seq)
      {
        // Discard the old extra msg
        extraMsgs.pop();
      }
      else
      {
        // Discard the old imu msg
        imuMsgs.pop();
      }
    }
    
    
  }


int main(int argc, char **argv)
{
  ros::init(argc, argv, "microstrain_restamper");
  ros::NodeHandle nh;
  ros::NodeHandle np("~");

  MicrostrainRestamper restamper(nh, np);

  ros::spin();

  return 0;
}
