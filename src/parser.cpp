/*
 * Copyright 2011 Virginia Polytechnic Institute and State University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 		* Redistributions of source code must retain the above copyright
 * 		  notice, this list of conditions and the following disclaimer.
 * 		* Redistributions in binary form must reproduce the above copyright
 * 		  notice, this list of conditions and the following disclaimer in the
 * 		  documentation and/or other materials provided with the distribution.
 * 		* Neither the name of the <organization> nor the
 * 		  names of its contributors may be used to endorse or promote products
 * 		  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *
 * parser.cpp
 *
 * This file contains the implementation for a PacketExtractor.  A packet
 * requires a sync packet, a header length, an index within the header to
 * find packet length, and an optional checksum. It will then send the packet
 * data to a user-defined PacketHandler (a subclass of PacketExtractor).
 *
 *  Created on: April 13, 2011
 *      Author: Kevin Stefanik (stefanik08@gmail.com)
 */

#include "parser.h"
#include <iostream>
#include <ros/ros.h>

#define MIN(a,b)	((a)<(b)?(a):(b))

bool vecEquals(const PacketExtractor::packet &v1,
			   const PacketExtractor::packet &v2,
			   const unsigned int len)
{
	for (unsigned int i=0; i<len; i++)
		if (v1[i] != v2[i])
			return false;
	return true;
}

PacketExtractor::PacketExtractor(unsigned int hdr_len, unsigned int pkt_len_ind,
		const packet &s, Checksum * c)
{
	if (pkt_len_ind >= hdr_len)
		throw pkt_len_ind;
	//copy the sync
	sync = s;
	hdr_storage.resize(hdr_len);
	len_ind = pkt_len_ind;
	hdr_ind = 0;
	chksm = c;
}
void PacketExtractor::addHandler(PacketHandler * h)
{
	hs.remove(h);
	hs.push_back(h);
}
void PacketExtractor::removeHandler(PacketHandler * h)
{
	hs.remove(h);
}

bool PacketExtractor::addData(const char * data, unsigned int len)
{
	bool result = false;
	for (unsigned int i = 0; i < len; i++)
	{
		if (addData(data[i]))
			result = true;
	}
	return result;
}
bool PacketExtractor::addData(char data)
{
	if (hdr_ind < hdr_storage.size())
	{
		hdr_storage[hdr_ind] = data;
		hdr_ind++;
		//loop to remove non-sync data from the beginning
		while ( !vecEquals(hdr_storage, sync, MIN(hdr_ind, sync.size() ))
			&& hdr_ind > 0)
		{
			hdr_ind--;
			for (unsigned int x=0; x<hdr_ind; x++)
				hdr_storage[x] = hdr_storage[x+1];
		}
		if (hdr_ind == hdr_storage.size()) //if we've found the header:
		{
			pkt_storage = hdr_storage; //store the header in the packet,
			//then resize the packet to the specified length, and fill with 1's
			unsigned int pkt_length = hdr_storage.size() +  pkt_storage[len_ind];
			if (chksm != 0)
			{
				pkt_length += chksm->length();
			}
			if(pkt_length>400)
			  {
			    ROS_WARN_STREAM("pkt_length="<<pkt_length);
			    pkt_length = 400;
			  }

			pkt_storage.resize(pkt_length, 0xFF);
			pkt_ind = hdr_ind;
		}
	}
	//if the whole packet hasn't been received, then continue storing it
	else if (pkt_ind < pkt_storage.size())
	{
		pkt_storage[pkt_ind] = data;
		pkt_ind++;
		if (pkt_ind == pkt_storage.size())
		{
			pkt_ind = 0;
			hdr_ind = 0;
			if (chksm == 0 || chksm->check(pkt_storage))
			{
				for (std::list<PacketHandler*>::iterator it = hs.begin();
					it != hs.end(); it++)
				{
					(*it)->handle(pkt_storage);
				}
				return true;
			}
			else
				return false;
		}
	}
	return false;
}
