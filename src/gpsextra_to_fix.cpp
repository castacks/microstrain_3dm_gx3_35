/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

// Converts our custom microstrain_3dm_gx3_35/GpsExtra message to gps_common/GPSFix so we can reuse utm conversion nodes

#include "ros/ros.h"
#include "gps_common/GPSFix.h"
#include "gps_common/GPSStatus.h"
#include <microstrain_3dm_gx3_35/GpsExtra.h>
#include <limits>

class GpsExtraToFix {

public:
  GpsExtraToFix(ros::NodeHandle nh, ros::NodeHandle np);
  ~GpsExtraToFix();
  void gpsExtraCallback(const microstrain_3dm_gx3_35::GpsExtra::ConstPtr& msg);

private:
  
  ros::NodeHandle nh_, np_;
  ros::Publisher gpsfix_pub_;
  ros::Subscriber gpsextra_sub_;

};
 
  // Constructor 
  GpsExtraToFix::GpsExtraToFix(ros::NodeHandle nh, ros::NodeHandle np):
    nh_(nh),
    np_(np)
  {
    gpsextra_sub_ = nh_.subscribe("input", 100, &GpsExtraToFix::gpsExtraCallback, this);
    gpsfix_pub_ = nh_.advertise<gps_common::GPSFix>("output", 100);
    
  }
  
  GpsExtraToFix::~GpsExtraToFix()
  {
  }


  void GpsExtraToFix::gpsExtraCallback(const microstrain_3dm_gx3_35::GpsExtra::ConstPtr& msg)
  {
    gps_common::GPSFix output;
    

    
    gps_common::GPSStatus status;
    
    status.header = msg->header;
    status.satellites_used = msg->fix_num_sats;
    //status.satellite_used_prn = ;
    //satellites_visible = ;
    //satellite_visible_prn = ;
    //satellite_visible_z = ;
    //satellite_visible_azimuth = ;
    //satellite_visible_snr = ;
    
    /* 
    Microstrain GPS Fix Information:
    Possible Fix Types values are:
    0x00 – 3D Fix
    0x01 – 2D Fix
    0x02 – Time Only
    0x03 – None
    0x04 – Invalid
    */
    
    /*
    GPS common GPS Status:
    Measurement status values are:
    int16 STATUS_NO_FIX=-1   # Unable to fix position
    int16 STATUS_FIX=0       # Normal fix
    int16 STATUS_SBAS_FIX=1  # Fixed using a satellite-based augmentation system
    int16 STATUS_GBAS_FIX=2  #          or a ground-based augmentation system
    int16 STATUS_DGPS_FIX=18 # Fixed with DGPS
    int16 STATUS_WAAS_FIX=33 # Fixed with WAAS
    */
    
    switch (msg->fix_type)
    {
      case 0x00:
      case 0x01:
        status.status = gps_common::GPSStatus::STATUS_FIX;
        break;
      case 0x02:
      case 0x03:
      case 0x04:
      default:
        status.status = gps_common::GPSStatus::STATUS_NO_FIX;
        break;
    }
    
    /*
    # Types of sources
    uint16 SOURCE_NONE=0 # No information is available
    uint16 SOURCE_GPS=1 # Using standard GPS location [only valid for position_source]
    uint16 SOURCE_POINTS=2 # Motion/orientation fix is derived from successive points
    uint16 SOURCE_DOPPLER=4 # Motion is derived using the Doppler effect
    uint16 SOURCE_ALTIMETER=8 # Using an altimeter
    uint16 SOURCE_MAGNETIC=16 # Using magnetic sensors
    uint16 SOURCE_GYRO=32 # Using gyroscopes
    uint16 SOURCE_ACCEL=64 # Using accelerometers
    */
    
    // I'm assuming that this is the case with this GPS receiver...
    status.motion_source = gps_common::GPSStatus::SOURCE_GPS;
    status.orientation_source = gps_common::GPSStatus::SOURCE_GPS;
    status.position_source = gps_common::GPSStatus::SOURCE_GPS;
    
    // Finished defining the status message...
    
    
    output.header = msg->header;
    output.status = status;
    
    output.latitude = msg->latitude;
    output.longitude = msg->longitude;
    output.altitude = msg->alt_ellipsoid; // ???
    
    output.track = msg->ned_heading;
    output.speed = msg->ned_ground_spd;
    output.climb = -1 * msg->ned_velocity.z;
    
    output.pitch = std::numeric_limits<double>::quiet_NaN();
    output.roll = std::numeric_limits<double>::quiet_NaN();
    output.dip = std::numeric_limits<double>::quiet_NaN();
    
    //output.time =
    
    output.gdop = msg->dop_geom;
    output.pdop = msg->dop_pos;
    output.hdop = msg->dop_horiz;
    output.vdop = msg->dop_vert;
    output.tdop = msg->dop_time;
    
    //output.err = 
    output.err_horz = msg->horizontal_acc;
    output.err_vert = msg->vertical_acc;
    output.err_track = msg->ned_head_acc;
    output.err_speed = msg->ned_spd_acc;
    output.err_climb = msg->ned_spd_acc;  // Is this a valid assumption?
    output.err_time = msg->clk_acc;
    
    output.err_pitch = 0;
    output.err_roll = 0;
    output.err_dip = 0;
    
    
    /*
    # Position covariance [m^2] defined relative to a tangential plane
    # through the reported position. The components are East, North, and
    # Up (ENU), in row-major order.
    */
    output.position_covariance[0] = pow(output.err_horz,2);
    output.position_covariance[4] = pow(output.err_horz,2);
    output.position_covariance[8] = pow(output.err_vert, 2);
    
    /*
    Pose covariance types:
    uint8 COVARIANCE_TYPE_UNKNOWN = 0
    uint8 COVARIANCE_TYPE_APPROXIMATED = 1
    uint8 COVARIANCE_TYPE_DIAGONAL_KNOWN = 2
    uint8 COVARIANCE_TYPE_KNOWN = 3
    */
    output.position_covariance_type = gps_common::GPSFix::COVARIANCE_TYPE_DIAGONAL_KNOWN;

  // Publish ros message
  gpsfix_pub_.publish(output);
  } 


int main(int argc, char **argv)
{
  ros::init(argc, argv, "microstrain_gps_fix");
  ros::NodeHandle nh;
  ros::NodeHandle np("~");

  GpsExtraToFix converter(nh, np);

  ros::spin();

  return 0;
}
