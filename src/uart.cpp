/*
 * Copyright 2011 Virginia Polytechnic Institute and State University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 		* Redistributions of source code must retain the above copyright
 * 		  notice, this list of conditions and the following disclaimer.
 * 		* Redistributions in binary form must reproduce the above copyright
 * 		  notice, this list of conditions and the following disclaimer in the
 * 		  documentation and/or other materials provided with the distribution.
 * 		* Neither the name of the <organization> nor the
 * 		  names of its contributors may be used to endorse or promote products
 * 		  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *
 * UART.cpp
 *
 * The purpose of this class is to abstract a serial port to a universal
 * asynchronous receiver/transmitter (UART) to a class which provides
 * synchronous write operations, and asynchronous reads.  All reads are handled
 * by new thread, which executes a "process" function of a UARTListener
 * object.  An arbitrary number of UARTListeners may be added to the UART. Any
 * read() errors are printed to std::cerr.
 *
 * This UART class is dependent on the cross-platform boost/asio library.
 * As such, it is portable to both Linux and Windows.  Development was 
 * performed using boost version 1.44.0.
 *
 *  Created on: Mar 15, 2011
 *      Author: Kevin Stefanik (stefanik08@gmail.com)
 */

#include "uart.h"
#include <iostream>
#include <ros/ros.h>
#include <cstring>
using namespace boost::asio;
using std::vector;

/**
 * Constructor - attempts to open the given serial port, if specified.
 * Will start second thread if successful.
 * @param dev is the serial port device name.
 * 		e.g. 	Windows: "COM1"
 * 				Linux: "/dev/ttyS0"
 * @param baud is the baudrate.  Some valid baud rates are (depending on
 * 		your port type):
 * 		300, 600, 1200, 1800, 2400, 4800, 7200, 9600, 14400, 19200, 38400,
 * 		57600, 115200, 230400, 460800, 921600
 * @param pcktSize - the value sent to the "read" command.  This acts as a
 * 		maximum value for the length of data given to UARTListener::process
 */
UART::UART(const std::string dev, unsigned int baud, int sz)
{
	pcktSize = sz;
	go = false;
	t = 0;
	port = new boost::asio::serial_port(service);
	useDev(dev, baud);
	
}

/**
 * Destructor - ends asynchronous read() thread, and closes the port.
 */
UART::~UART()
{
	stop();
	if (port->is_open())
		port->close();
	delete port;
}

void UART::close()
{
	stop();
	if (port->is_open())
	{
		port->close();
	}
}

/**
 * useDev(char*, unsigned int) - This allows the user to change the port
 * 		on-the-fly.  It cancels any current read operations in process.
 * @param dev is the serial port device name.
 * 		e.g. 	Windows: "COM1"
 * 				Linux: "/dev/ttyS0"
 * @param baud is the baudrate.  Some valid baud rates are (depending on
 * 		your port type):
 * 		300, 600, 1200, 1800, 2400, 4800, 7200, 9600, 14400, 19200, 38400,
 * 		57600, 115200, 230400, 460800, 921600
 */
bool UART::useDev(const std::string device, unsigned int baud)
{
	if (go)
		stop();
	
	if (port->is_open())
		port->close();

	boost::system::error_code ecc = boost::system::error_code();
	  port->open(device, ecc);
	if (port->is_open())
	{
		port->set_option(serial_port_base::baud_rate(baud));
		port->set_option(serial_port_base::flow_control(
				serial_port_base::flow_control::none));
		port->set_option(serial_port_base::parity(serial_port_base::parity::none));
		port->set_option(serial_port_base::stop_bits(
				serial_port_base::stop_bits::one));
		port->set_option(serial_port_base::character_size(8));
		start();
	}
	return port->is_open();
}

/**
 * isOpen()
 * @return whether the port has been successfully opened
 */
bool UART::isOpen()
{
	return port->is_open();
}

/**
 * write(char*, unsigned int) - writes the byte array to the port.  This
 * 		does not return until all bytes are written.
 * @param data is the byte array
 * @param len is the number of bytes to write.  If unspecified, or -1,
 * 		it will use the value returned by strlen("") in <cstring>
 * @return true if successful (port is open), false otherwise
 */
using namespace std;
bool UART::write(const char* data, int len)
{
	if (!port->is_open())
		return false;

	unsigned int l = len;
	if (len < 0)
		l = strlen(data);

	cout << hex;
	for (unsigned int i=0;i<len;i++)
		cout << (int)data[i] << ",";
	cout << endl << dec;
//	variableWrite(data,len);
	return port->write_some(buffer(data, l)) == l;
}

void UART::variableWrite(const char *data,int len)
{
  std::cout<<"char writeBuffer[] = {";
  for(int i=0;i<len;++i)
    {
      std::cout<<"0x"<<hex<<((int)data[i]);
      if(i<len-1)
	std::cout<<",";
    }
  std::cout<<"};";

}


void UART::variableWrite(const vector<char> &data)
{
  std::cout<<"char writeBuffer[] = {";
  for(int i=0;i<(int)data.size();++i)
    {
      std::cout<<"0x"<<hex<<((int)data[i]);
      if(i<(int)(data.size()-1))
	std::cout<<",";
    }
  std::cout<<"};";
}

/**
 * write(char*, unsigned int) - writes the byte array to the port.  This
 * 		does not return until all bytes are written.
 * @param data is the byte array
 * @return true if successful (port is open), false otherwise
 */
bool UART::write(const vector<char> &data)
{
 // variableWrite(data);
	if (!port->is_open())
		return false;
	
	cout << hex;
	for (unsigned int i=0;i<data.size();i++)
		cout << (int)data[i] << ",";
	cout << endl << dec;

	return port->write_some(buffer(data)) == data.size();
}

/**
 * addUARTList(UARTListener*) - This function adds a user's UARTListener
 * 		pointer to this UART.  Note that each pointer MUST be valid until
 * 		the destructor of UART is called.  Multiple pointers may be added.
 */
void UART::addUARTList(UARTListener* list)
{
	lists.push_back(list);
}

/**
 * removeUARTList(UARTListener*) - This function removes a user's
 * 		UARTListener pointer to this UART.
 */
void UART::removeUARTList(UARTListener* list)
{
	lists.remove(list);
}

/********* begin private function definitions ************/
void UART::start()
{
	if (port->is_open())
	{
		go = true;
		t = new boost::thread(boost::bind(&UART::run, this));
	}
}
void UART::stop()
{
	go = false;
	if (port && port->is_open())
	{
		try {
		port->cancel();
		} catch (boost::system::system_error) { }
	}
	if (t)
	{
		t->join();
	}
}
void UART::readHandler(const boost::system::error_code& error,
		std::size_t bytes_transferred)
{
	if (error)
	{
	  ROS_ERROR_STREAM("UART: Error on read!");
		return;
	}
	if (data)
	{
		for (std::list<UARTListener*>::iterator it = lists.begin(); it
				!= lists.end(); it++)
		{
			(*it)->process(data, bytes_transferred);
		}
	}
}
void UART::run()
{
	data = new char[pcktSize];
	while (port->is_open() && go)
	{
		service.reset();
		port->async_read_some(buffer(&data[0], pcktSize), boost::bind(
				&UART::readHandler, this, placeholders::error,
				placeholders::bytes_transferred));
		service.run();
	}
	delete[] data;
}

