/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/*
 * Copyright 2011 Virginia Polytechnic Institute and State University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 		* Redistributions of source code must retain the above copyright
 * 		  notice, this list of conditions and the following disclaimer.
 * 		* Redistributions in binary form must reproduce the above copyright
 * 		  notice, this list of conditions and the following disclaimer in the
 * 		  documentation and/or other materials provided with the distribution.
 * 		* Neither the name of the <organization> nor the
 * 		  names of its contributors may be used to endorse or promote products
 * 		  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *
 * main.cpp
 *
 *  Created on: Apr 13, 2011
 *      Author: kevin

 * Edited by sebastian scherer
 * Biases added by sezal jain
 */


#include "ms3dmgx335.h"
#include <iostream>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <microstrain_3dm_gx3_35/ImuExtra.h>
#include <microstrain_3dm_gx3_35/GpsExtra.h>


using namespace std;

using namespace microstrain::gx335;

class MyHandler : public GX3Handler
{
public:
  static const int INITIAL_OFFSET_SAMPLE_COUNT = 200;
  static const int INITIAL_OFFSET_SAMPLE_COUNT_GPS = 10;
  ros::NodeHandle imu_node_handle;
  ros::Publisher imu_data_pub;
  ros::Publisher imu_data_filtered_pub;	
  ros::Publisher imu_extra_pub;
  ros::Publisher gps_extra_pub;
  int receivedgps;
  int receivedgpsSent;
  int receivedimu;
  int receivedimuSent;

  double imuTimeOffset;
  int    imuTimeCount;
  double gpsTimeOffset;
  int    gpsTimeCount;

  double cutOff;
  double outputFrequency; // frequency of data filtered from initial 1000Hz input
  bool scaled_accel_init;
  bool scaled_gyro_init;
	bool imu_filtering;
  int counter;
  //Buffers
  sensor_msgs::Imu imu_data;
  microstrain_3dm_gx3_35::ImuExtra imu_extra;
  microstrain_3dm_gx3_35::GpsExtra gps_extra;
  sensor_msgs::Imu imu_data_filtered; //data filtered from initial 1000Hz input
  sensor_msgs::Imu imu_data_filtered_prev; // the y(n-1) for filter

  double acceleration_xyz[10][3];
  double gyro_xyz[10][3];
  int accel_index,gyro_index;
  MyHandler():
    GX3Handler(),
    receivedgps(0),
    receivedgpsSent(0),    
    receivedimu(0),
    receivedimuSent(0),
    imuTimeOffset(0),
    imuTimeCount(0),
    gpsTimeOffset(0),
    gpsTimeCount(0),
    cutOff(30.0),
    outputFrequency(100.0),
    scaled_accel_init(false),
    scaled_gyro_init(false),
		imu_filtering(false),
		counter(0),
		accel_index(0),
		gyro_index(0)
  {
    imu_data_pub = imu_node_handle.advertise<sensor_msgs::Imu>("/microstrain/imu", 50);
	//imu_data_filtered_pub = imu_node_handle.advertise<sensor_msgs::Imu>("/microstrain/imu_filtered", 50);
    imu_extra_pub = imu_node_handle.advertise<microstrain_3dm_gx3_35::ImuExtra>("/microstrain/imu_extra", 50);
    gps_extra_pub = imu_node_handle.advertise<microstrain_3dm_gx3_35::GpsExtra>("/microstrain/gps", 4);
  }
  virtual float filter(const float &x_n, const float &y_nminus1)
  {

	  float tau_s=1/1000.0; //sampling interval
	  float tau_f=1/(3.14159*outputFrequency);  // according to the nyquist theorem (cutting off frequencies resulting in aliasing)
	  //float tau_f=1/(3.14159*cutOff);
	  float alpha=(tau_f)/(tau_f+tau_s);
	  float result=alpha*y_nminus1+(1-alpha)*x_n;
	  return result;
  }
  virtual void filteravg()
  {
	  double sum[2][3];
      for(int j=0;j<3;j++)
      {
          sum[0][j] = 0.0;
          sum[1][j] = 0.0;
      }
	  for(int i=0;i<10;i++)
	  {
		  for(int j=0;j<3;j++)
		  {
			  sum[0][j]+=acceleration_xyz[i][j];
			  sum[1][j]+=gyro_xyz[i][j];
//			cout<< acceleration_xyz[i][j]<< "   "<<gyro_xyz[i][j] << "   ";
		  }
//	cout<<endl;

	  }
//	cout<<"new set "<<endl;
//	cout<<"sums are "<<sum[0][0]<<" , "<<sum[1][0]<<" , "<<sum[0][1]<<" , "<<sum[1][1]<<" , "<<sum[0][2]<<" , "<<sum[1][2]<<endl;
	  imu_data_filtered.linear_acceleration.x=sum[0][0]/10.0;
	  imu_data_filtered.linear_acceleration.y=sum[0][1]/10.0;
	  imu_data_filtered.linear_acceleration.z=sum[0][2]/10.0;
	  imu_data_filtered.angular_velocity.x=sum[1][0]/10.0;
	  imu_data_filtered.angular_velocity.y=sum[1][1]/10.0;
	  imu_data_filtered.angular_velocity.z=sum[1][2]/10.0;
  }
  virtual geometry_msgs::Vector3 convert(const ahrs::_xyzfloats &a)
  {
    geometry_msgs::Vector3 res;
    res.x = a.x;
    res.y = a.y;
    res.z = a.z;
    return res;
  }
  virtual void quaternion(const ahrs::quaternion::t &t)
  {
    receivedimu++;
    ROS_DEBUG_STREAM(" Quaternion: "<<t.q[0]<<" "<<t.q[1]<<" "<<t.q[2]<<" "<<t.q[3]);
    imu_data.orientation.x = t.q[1];
    imu_data.orientation.y = t.q[2];
    imu_data.orientation.z = t.q[3];
    imu_data.orientation.w = t.q[0];
// the orientation is not being filtered currently /TODO
    imu_data_filtered.orientation.x = t.q[1];
    imu_data_filtered.orientation.y = t.q[2];
    imu_data_filtered.orientation.z = t.q[3];
    imu_data_filtered.orientation.w = t.q[0];
    
  }
  virtual void scaled_accel(const ahrs::scaled_accel::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("Scaled accel: " << d.x << ", " << d.y<< ", " << d.z);
    imu_data.linear_acceleration = convert(d);
    //Units in g convert to m/s^2
    imu_data.linear_acceleration.x *= 9.80665;
    imu_data.linear_acceleration.y *= 9.80665;
    imu_data.linear_acceleration.z *= 9.80665;
    acceleration_xyz[accel_index][0]=imu_data.linear_acceleration.x;
    acceleration_xyz[accel_index][1]=imu_data.linear_acceleration.y;
    acceleration_xyz[accel_index][2]=imu_data.linear_acceleration.z;
    if(accel_index==9&&!scaled_accel_init)scaled_accel_init=true;
    accel_index=(accel_index+1)%10; //rolling buffer for elements
// for the low pass filter
 /*   if(scaled_accel_init)
    {
		imu_data_filtered.linear_acceleration.x = filter(imu_data.linear_acceleration.x,imu_data_filtered_prev.linear_acceleration.x);
		imu_data_filtered.linear_acceleration.y = filter(imu_data.linear_acceleration.y,imu_data_filtered_prev.linear_acceleration.y);
		imu_data_filtered.linear_acceleration.z = filter(imu_data.linear_acceleration.z,imu_data_filtered_prev.linear_acceleration.z);
    }
	imu_data_filtered_prev.linear_acceleration.x = imu_data_filtered.linear_acceleration.x;
	imu_data_filtered_prev.linear_acceleration.y = imu_data_filtered.linear_acceleration.y;
	imu_data_filtered_prev.linear_acceleration.z = imu_data_filtered.linear_acceleration.z;
	scaled_accel_init=true;*/
  }
  virtual void scaled_gyro(const ahrs::scaled_gyro::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM( "Scaled gyro: " << d.x << ", " << d.y << ", " << d.z);
    imu_data.angular_velocity = convert(d);
    gyro_xyz[gyro_index][0]=imu_data.angular_velocity.x;
    gyro_xyz[gyro_index][1]=imu_data.angular_velocity.y;
    gyro_xyz[gyro_index][2]=imu_data.angular_velocity.z;
    if(gyro_index==9&&!scaled_gyro_init)scaled_gyro_init=true;
    gyro_index=(gyro_index+1)%10;
// for the low pass filter
   /* if(scaled_gyro_init)
    {
		imu_data_filtered.angular_velocity.x = filter(imu_data.angular_velocity.x,imu_data_filtered_prev.angular_velocity.x);
		imu_data_filtered.angular_velocity.y = filter(imu_data.angular_velocity.y,imu_data_filtered_prev.angular_velocity.y);
		imu_data_filtered.angular_velocity.z = filter(imu_data.angular_velocity.z,imu_data_filtered_prev.angular_velocity.z);
    }
	imu_data_filtered_prev.angular_velocity.x = imu_data_filtered.angular_velocity.x;
	imu_data_filtered_prev.angular_velocity.y = imu_data_filtered.angular_velocity.y;
	imu_data_filtered_prev.angular_velocity.z = imu_data_filtered.angular_velocity.z;
	scaled_gyro_init=true;*/
  }
  virtual void scaled_mag(const ahrs::scaled_mag::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM( "Magnetometer: " << d.x << ", " << d.y << ", " << d.z);
    imu_extra.magnetometer = convert(d);

  }
  virtual void internal_time(const ahrs::internal_time::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("Internal time: " << d);
     imu_extra.internal_time = d;
    if(imuTimeCount<INITIAL_OFFSET_SAMPLE_COUNT)
      {
	double diff = ros::Time::now().toSec() - ((double)d/((double)62500.0));
	imuTimeOffset = (imuTimeOffset * imuTimeCount + diff)/ (imuTimeCount+1);
	imuTimeCount++;
	if(imuTimeCount % 4 ==0)
	  ROS_INFO_STREAM("IMU Time offset: "<<imuTimeOffset);
      }
    
  }
  virtual void beacon_time(const ahrs::beacon_time::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("Beacon time ");
    imu_extra.beacon_status = d.status;
    imu_extra.beacon_pps_cnt = d.pps_cnt;
    imu_extra.beacon_ns_cnt = d.ns_cnt;
  }
  virtual void gps_time(const ahrs::gps_time::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("GPS time: " << d.time_of_wk << ", " << d.wk_num<< ", flags: " << d.valid_flags);
    imu_extra.gps_time_of_wk = d.time_of_wk;
    imu_extra.gps_week       = d.wk_num;
    imu_extra.gps_valid      = d.valid_flags;
  }

  virtual void delta_theta(const ahrs::delta_theta::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("Delta Theta: " << d.x << ", " << d.y << ", " << d.z);
    imu_extra.delta_theta = convert(d);
  }
  virtual void delta_velocity(const ahrs::delta_velocity::t &d)
  {
    receivedimu++;
    ROS_DEBUG_STREAM("Delta Velocity: " << d.x << ", " << d.y<< ", " << d.z);
    imu_extra.delta_velocity = convert(d);
  }

  /* The gps data components */
  
  virtual void llh_position(const gps::llh_position::t &d)
  {
    receivedgps++;
    ROS_DEBUG_STREAM("LLH Position: " << d.latitude << ", " << d.longitude<< ", " << d.alt_ellipsoid << ", " << d.alt_msl << ", " << d.horizontal_acc << ", " << d.vertical_acc << ", " << d.valid_flags);
    gps_extra.latitude       = d.latitude;
    gps_extra.longitude      = d.longitude;
    gps_extra.alt_ellipsoid  = d.alt_ellipsoid;
    gps_extra.alt_msl        = d.alt_msl;
    gps_extra.horizontal_acc = d.horizontal_acc;
    gps_extra.vertical_acc   = d.vertical_acc;
    gps_extra.llh_valid      = d.valid_flags;
  }

  virtual void ned_velocity(const gps::ned_velocity::t &d) 
  {
    receivedgps++;
    ROS_DEBUG_STREAM(" ned vel ");
    gps_extra.ned_velocity.x = d.north;
    gps_extra.ned_velocity.y = d.east;
    gps_extra.ned_velocity.z = d.down;
    gps_extra.ned_speed      = d.speed;
    gps_extra.ned_ground_spd = d.ground_spd;
    gps_extra.ned_heading    = d.heading;
    gps_extra.ned_spd_acc    = d.spd_acc;
    gps_extra.ned_head_acc   = d.head_acc;
    gps_extra.ned_valid      = d.valid_flags;
    
  }
  virtual void gps_time(const gps::gps_time::t &d) 
  {
    receivedgps++;
    ROS_DEBUG_STREAM(" gps time ");
    gps_extra.gps_time_of_wk = d.time_of_wk;
    gps_extra.gps_week       = d.wk_num;
    gps_extra.gps_valid      = d.valid_flags;
    
    if(gpsTimeCount<INITIAL_OFFSET_SAMPLE_COUNT_GPS)
      {
		double diff = ros::Time::now().toSec() - d.time_of_wk;
		gpsTimeOffset = (gpsTimeOffset * gpsTimeCount + diff)/ (gpsTimeCount+1);
		gpsTimeCount++;
		ROS_INFO_STREAM("GPS Time offset: "<<gpsTimeOffset);
      }

    
  }
  virtual void gps_fix(const gps::gps_fix::t &d) 
  {
    receivedgps++;
    ROS_DEBUG_STREAM(" gps fix ");
    gps_extra.fix_type     = d.fix_type;
    gps_extra.fix_num_sats = d.num_sats;
    gps_extra.fix_flags    = d.fix_flags;
    gps_extra.fix_valid    = d.valid_flags;
  }
  virtual void dop_data(const gps::dop_data::t &d) 
  {
    receivedgps++;
    ROS_DEBUG_STREAM("dop ");
    gps_extra.dop_geom = d.geom;
    gps_extra.dop_pos = d.pos;
    gps_extra.dop_horiz = d.horiz;
    gps_extra.dop_vert = d.vert;
    gps_extra.dop_time = d.time;
    gps_extra.dop_northing = d.northing;
    gps_extra.dop_easting = d.easting;
    gps_extra.dop_valid = d.valid_flags;
  }
  virtual void utc_time(const gps::utc_time::t &d) 
  {
    receivedgps++;
    ROS_DEBUG_STREAM(" utc time ");
    gps_extra.utc_year   = d.year;
    gps_extra.utc_month  = d.month;
    gps_extra.utc_hour   = d.hour;
    gps_extra.utc_minute = d.minute;
    gps_extra.utc_second = d.second;
    gps_extra.utc_millis = d.millis;
    gps_extra.utc_valid  = d.valid_flags;
  }
  virtual void clock_info(const gps::clock_info::t &d) 
  {
    receivedgps++;
    ROS_DEBUG_STREAM(" clock info ");
    gps_extra.clk_bias  = d.clk_bias;
    gps_extra.clk_drift = d.clk_drift;
    gps_extra.clk_acc   = d.acc;
    gps_extra.clk_valid = d.valid_flags;
  }
 
  virtual void eop() 
  {
    if(gpsTimeCount>=INITIAL_OFFSET_SAMPLE_COUNT_GPS)
      if(receivedgps != receivedgpsSent)
      {
	      ROS_DEBUG_STREAM("EOP. Sending gps");
	      receivedgpsSent = receivedgps;
	      gps_extra.header.stamp = ros::Time(gps_extra.gps_time_of_wk+ gpsTimeOffset); //ros::Time::now();
	      gps_extra.header.frame_id = "microstrain";
	      gps_extra_pub.publish(gps_extra);
      }
    if(imuTimeCount>=INITIAL_OFFSET_SAMPLE_COUNT)
      if(receivedimu != receivedimuSent)
      {
	      counter++;
      //if (counter%10==0){
	      ROS_DEBUG_STREAM("EOP. Sending imu");
	      receivedimuSent = receivedimu;
	      if(!imu_filtering){
		      imu_data.header.stamp = ros::Time::now(); //ros::Time(((double)imu_extra.internal_time/62500.0)+ imuTimeOffset);
		      imu_data.header.frame_id = "microstrain";
		      imu_extra.header.stamp = ros::Time::now();//Just in case we need to fix the time stamps later use computer time here.
		      imu_extra.header.frame_id = "microstrain";
		      imu_data_pub.publish(imu_data);
		      imu_extra_pub.publish(imu_extra);
	      }
	      //ROS_ERROR_STREAM(ros::Time::now().toSec());
	      else if(scaled_gyro_init&&scaled_accel_init){
		      imu_data_filtered.header.frame_id = "microstrain";
		      imu_data_filtered.header.stamp = ros::Time::now(); //ros::Time(((double)imu_extra.internal_time/62500.0)+ imuTimeOffset);
		      filteravg();
		      if (counter%10==0)imu_data_pub.publish(imu_data_filtered);
	      }
      //}
      }
  }
};


int main(int argc, char **argv)
{
	//Now start ROS
	ros::init(argc, argv, "microstrain_3dm_gx3_35");
	ros::NodeHandle n;
	ros::NodeHandle np("~");
	std::string portname;
	double orientation_stdev;
	double angular_velocity_stdev;
	double linear_acceleration_stdev; 
	double gyro_bias_x,gyro_bias_y,gyro_bias_z;
	int rate_divider;  // the divider for sampling frequency Fs=1000/rate_divider
	bool basic_imu_streaming;
  
	if(!np.getParam("port",portname))
	  {
	    ROS_ERROR_STREAM("Port not defined");
	    return -1;
	  }
	  
	if(!np.getParam("orientation_stdev",orientation_stdev))
	  {
	    ROS_ERROR_STREAM("Orientation standard deviation not defined");
	    return -1;
	  }
	  
	if(!np.getParam("angular_velocity_stdev",angular_velocity_stdev))
	  {
	    ROS_ERROR_STREAM("Angular velocity standard deviation not defined");
	    return -1;
	  }

	if(!np.getParam("linear_acceleration_stdev",linear_acceleration_stdev))
	  {
	    ROS_ERROR_STREAM("Linear acceleration standard deviation not defined");
	    return -1;
	  }
	if(!n.getParam("/microstrain_3dm_gx3_35/port",portname))
	  {
	    ROS_ERROR_STREAM("Port not defined");
	    return -1;
	  }
	np.param<int>("rate_divider",rate_divider,2);
	np.param("gyro_bias_x",gyro_bias_x,0.0);
	np.param("gyro_bias_y",gyro_bias_y,0.0);
	np.param("gyro_bias_z",gyro_bias_z,0.0);
	np.param("basic_imu_streaming",basic_imu_streaming,false);
	MyHandler h;
	np.param("imu_filtering",h.imu_filtering,false);
	SerialGX335 gx3(portname);
	gx3.setGX3Handler(&h);
	if (!gx3.isGood())
	{
	  ROS_ERROR_STREAM("port not opened!");
	  return -1;
	}
	ROS_INFO_STREAM("successfully opened port!");
	
	//Setup the messages we want to send
	h.imu_data.orientation_covariance[0] = pow(orientation_stdev,2);
	h.imu_data.orientation_covariance[1] = 0;
	h.imu_data.orientation_covariance[2] = 0;
	h.imu_data.orientation_covariance[3] = 0;
	h.imu_data.orientation_covariance[4] = pow(orientation_stdev,2);
	h.imu_data.orientation_covariance[5] = 0;
	h.imu_data.orientation_covariance[6] = 0;
	h.imu_data.orientation_covariance[7] = 0;
	h.imu_data.orientation_covariance[8] = pow(orientation_stdev,2);
  
	h.imu_data.angular_velocity_covariance[0] = pow(angular_velocity_stdev,2);
	h.imu_data.angular_velocity_covariance[1] = 0;
	h.imu_data.angular_velocity_covariance[2] = 0;
	h.imu_data.angular_velocity_covariance[3] = 0;
	h.imu_data.angular_velocity_covariance[4] = pow(angular_velocity_stdev,2);
	h.imu_data.angular_velocity_covariance[5] = 0;
	h.imu_data.angular_velocity_covariance[6] = 0;
	h.imu_data.angular_velocity_covariance[7] = 0;
	h.imu_data.angular_velocity_covariance[8] = pow(angular_velocity_stdev,2);

	h.imu_data.linear_acceleration_covariance[0] = pow(linear_acceleration_stdev,2);
	h.imu_data.linear_acceleration_covariance[1] = 0;
	h.imu_data.linear_acceleration_covariance[2] = 0;
	h.imu_data.linear_acceleration_covariance[3] = 0;
	h.imu_data.linear_acceleration_covariance[4] = pow(linear_acceleration_stdev,2);
	h.imu_data.linear_acceleration_covariance[5] = 0;
	h.imu_data.linear_acceleration_covariance[6] = 0;
	h.imu_data.linear_acceleration_covariance[7] = 0;
	h.imu_data.linear_acceleration_covariance[8] = pow(linear_acceleration_stdev,2);

	
	std::vector<char> ds,ds2,r,r2;
	if(basic_imu_streaming){
		ds.push_back( microstrain::gx335::ahrs::quaternion::desc);r.push_back(rate_divider);
		ds.push_back( microstrain::gx335::ahrs::scaled_accel::desc);r.push_back(rate_divider);
		ds.push_back( microstrain::gx335::ahrs::scaled_gyro::desc);r.push_back(rate_divider);
		ds.push_back( microstrain::gx335::ahrs::internal_time::desc);r.push_back(rate_divider);
	}
	else{
		ds.push_back( microstrain::gx335::ahrs::quaternion::desc);r.push_back(rate_divider);
		ds.push_back( microstrain::gx335::ahrs::scaled_accel::desc);r.push_back(rate_divider);
		ds.push_back( microstrain::gx335::ahrs::scaled_gyro::desc);r.push_back(rate_divider);
		ds.push_back( microstrain::gx335::ahrs::scaled_mag::desc);r.push_back(rate_divider);
		ds.push_back( microstrain::gx335::ahrs::internal_time::desc);r.push_back(rate_divider);
		ds.push_back( microstrain::gx335::ahrs::beacon_time::desc);r.push_back(rate_divider);
		ds.push_back( microstrain::gx335::ahrs::gps_time::desc);r.push_back(rate_divider);
		ds2.push_back( microstrain::gx335::gps::llh_position::desc);r2.push_back(1);
		ds2.push_back( microstrain::gx335::gps::ned_velocity::desc);r2.push_back(1);
		ds2.push_back( microstrain::gx335::gps::gps_time::desc);r2.push_back(1);
	}
	//ds.push_back( microstrain::gx335::ahrs::delta_theta::desc);r.push_back(10);
	//ds.push_back( microstrain::gx335::ahrs::delta_velocity::desc);r.push_back(10);
	//ds2.push_back( microstrain::gx335::gps::gps_fix::desc);r2.push_back(1);
	//ds2.push_back( microstrain::gx335::gps::dop_data::desc);r2.push_back(1);
	//ds2.push_back( microstrain::gx335::gps::utc_time::desc);r2.push_back(1);
	//ds2.push_back( microstrain::gx335::gps::clock_info::desc);r2.push_back(4);
// Don't send config packets for now:
	gx3.set_ahrs(ds, r);
	gx3.set_gps(ds2, r2);
	//send gyro bias if it is unequal to zero. the float values are converted into hex in set_gyro_bias
	std::vector<float> ds3;
	ds3.push_back(gyro_bias_x);
	ds3.push_back(gyro_bias_y);
	ds3.push_back(gyro_bias_z);
	if(gyro_bias_x!=0.0||gyro_bias_y!=0.0||gyro_bias_z!=0.0) gx3.set_gyro_bias(ds3);
	//Spin and wait
	ros::spin();
	return 0;
}
