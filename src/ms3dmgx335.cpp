/*
 * Copyright 2011 Virginia Polytechnic Institute and State University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 		* Redistributions of source code must retain the above copyright
 * 		  notice, this list of conditions and the following disclaimer.
 * 		* Redistributions in binary form must reproduce the above copyright
 * 		  notice, this list of conditions and the following disclaimer in the
 * 		  documentation and/or other materials provided with the distribution.
 * 		* Neither the name of the <organization> nor the
 * 		  names of its contributors may be used to endorse or promote products
 * 		  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * ms3dmgx335.cpp
 *
 * This contains the interfaces for each class involved with 3DM-GX3-35 parsing.
 *
 * GX3Handler - the user implements this, user functions are automatically
 * 		called from a parallel thread when new data is received.  This is the
 * 		same thread for the "read" function, so any blocking will prevent any
 * 		future reads.
 * GX3PacketHandler - performs the "meat" of any parsing.  Use the "setHandler"
 * 		function to change the GX3Handler that is used.
 * Fletcher16 - calculates the checksums for the parser, and appends checksums
 * 		for writing data
 * GX3PacketExtractor - inherits from PacketExtractor, and automatically sets
 * 		up a GX3PacketHandler to handle packets (feeds through the "setHandler"
 * 		function)
 * SerialGX335 - inherits from GX3PacketExtractor, and incorporates the UART
 * 		class, along with setting both AHRS and GPS packets.
 *
 *
 *  Created on: April 13, 2011
 *      Author: Kevin Stefanik (stefanik08@gmail.com)
 */

#include "ms3dmgx335.h"
#include <iostream>
#include <ros/ros.h>

using std::cout;
using std::endl;
using std::hex;
using std::dec;

using namespace microstrain::gx335;

const char GX3PacketExtractor::sync_data[2] = {0x75, 0x65};
const packet GX3PacketExtractor::sync_pkt(sync_data, (sync_data+1));
Fletcher16 GX3PacketExtractor::fletcher;

ros::Time one;
GX3PacketHandler::GX3PacketHandler(GX3Handler * h)
{
	handler = h;
}

void GX3PacketHandler::handle(const packet &p)
{
	if (handler == 0) //no reason to parse data
		return;
	//ros::Time two=ros::Time::now();
	//ROS_ERROR_STREAM((two-one).toSec());
	//one=two;
	char len, desc=p[2], field_desc;
	packet data(p.begin()+4, p.end()); //remove header
	//good fields to parse
	while (data.size() > 2 && data.size() >= (unsigned char)data[0])
	{
		len = data[0]; //extract the length of the field
		field_desc = data[1];
		//remove header:
		data.erase(data.begin(), data.begin()+2); //skip field header
		switch (desc) //determine the descriptor set (AHRS, GPS, etc...)
		{
		case ahrs::desc:
			parseAhrs(data, field_desc); //parse the field
			break;
		case gps::desc:
			parseGps(data, field_desc); //parse the field
			break;
		case cmd::desc:
			parseCmd(data, field_desc);
			break;
		/*default:
			cout << hex;
			for (unsigned int i=0; i<p.size(); i++)
			{
				cout <<(unsigned int)p[i]<<",";
			}
			cout << endl << hex;*/
		}
		//remove the rest of the field from data:
		data.erase(data.begin(), data.begin()+len-2);
	}
	handler->eop();
}
void GX3PacketHandler::parseAhrs(packet &p, const char &desc)
{
	switch (desc)
	{
	case ahrs::raw_accel::desc:
		{if (p.size() < ahrs::raw_accel::len-2) return;
		ahrs::raw_accel::t tmp; extract3floats(p, (float*)&tmp);
		handler->raw_accel(tmp);}
		break;
	case ahrs::raw_gyro::desc:
		{if (p.size() < ahrs::raw_gyro::len-2) return;
		ahrs::raw_gyro::t tmp; extract3floats(p, (float*)&tmp);
		handler->raw_gyro(tmp);}
		break;
	case ahrs::raw_mag::desc:
		{if (p.size() < ahrs::raw_mag::len-2) return;
		ahrs::raw_mag::t tmp; extract3floats(p, (float*)&tmp);
		handler->raw_mag(tmp);}
		break;
	case ahrs::scaled_accel::desc:
		{if (p.size() < ahrs::scaled_accel::len-2) return;
		ahrs::scaled_accel::t tmp; extract3floats(p, (float*)&tmp);
		handler->scaled_accel(tmp);}
		break;
	case ahrs::scaled_gyro::desc:
		{if (p.size() < ahrs::scaled_gyro::len-2) return;
		ahrs::scaled_gyro::t tmp; extract3floats(p, (float*)&tmp);
		handler->scaled_gyro(tmp);}
		break;
	case ahrs::scaled_mag::desc:
		{if (p.size() < ahrs::scaled_mag::len-2) return;
		ahrs::scaled_mag::t tmp; extract3floats(p, (float*)&tmp);
		handler->scaled_mag(tmp);}
		break;
	case ahrs::delta_theta::desc:
		{if (p.size() < ahrs::delta_theta::len-2) return;
		ahrs::delta_theta::t tmp; extract3floats(p, (float*)&tmp);
		handler->delta_theta(tmp);}
		break;
	case ahrs::delta_velocity::desc:
		{if (p.size() < ahrs::delta_velocity::len-2) return;
		ahrs::delta_velocity::t tmp; extract3floats(p, (float*)&tmp);
		handler->delta_velocity(tmp);}
		break;
	case ahrs::orien_mat::desc:
		{if (p.size() < ahrs::orien_mat::len-2) return;
		ahrs::orien_mat::t tmp; extractNfloats(p, (float*)&tmp, 9);
		handler->orien_mat(tmp);}
		break;
	case ahrs::quaternion::desc:
		{if (p.size() < ahrs::quaternion::len-2) return;
		ahrs::quaternion::t tmp; extractNfloats(p, (float*)&tmp, 4);
		handler->quaternion(tmp);}
		break;
	case ahrs::orien_update::desc:
		{if (p.size() < ahrs::orien_update::len-2) return;
		ahrs::orien_update::t tmp; extractNfloats(p, (float*)&tmp, 9);
		handler->orien_update(tmp);}
		break;
	case ahrs::euler_angles::desc:
		{if (p.size() < ahrs::euler_angles::len-2) return;
		ahrs::euler_angles::t tmp; extract3floats(p, (float*)&tmp);
		handler->euler_angles(tmp);
		break;}
	case ahrs::internal_time::desc:
		{if (p.size() < ahrs::internal_time::len-2) return;
		ahrs::internal_time::t tmp; extract<ahrs::internal_time::t>(p, tmp);
		handler->internal_time(tmp);
		break;}
	case ahrs::beacon_time::desc:
		{if (p.size() < ahrs::beacon_time::len-2) return;
		ahrs::beacon_time::t tmp;
		tmp.status = p[0];
		packet d(p.begin()+1, p.begin()+5); extract<unsigned int>(d, tmp.pps_cnt);
		d.assign(p.begin()+5, p.begin()+9); extract<unsigned int>(d, tmp.ns_cnt);
		handler->beacon_time(tmp);
		break;}
	case ahrs::stab_mag::desc:
		{if (p.size() < ahrs::stab_mag::len-2) return;
		ahrs::stab_mag::t tmp; extract3floats(p, (float*)&tmp);
		handler->stab_mag(tmp);}
		break;
	case ahrs::stab_accel::desc:
		{if (p.size() < ahrs::stab_accel::len-2) return;
		ahrs::stab_accel::t tmp; extract3floats(p, (float*)&tmp);
		handler->stab_accel(tmp);
		break;}
	case ahrs::gps_time::desc:
		{if (p.size() < ahrs::gps_time::len-2) return;
		ahrs::gps_time::t tmp; //extract<ahrs::gps_time::t>(p, tmp);
		packet d(p.begin(), p.begin()+8); extract<double>(d, tmp.time_of_wk);
		d.assign(p.begin()+8, p.begin()+10); extract<unsigned short>(d, tmp.wk_num);
		d.assign(p.begin()+10, p.begin()+12); extract<unsigned short>(d, tmp.valid_flags);
		handler->gps_time(tmp);
		break;}
	}
}
void GX3PacketHandler::parseGps(packet &p, const char &desc)
{
	switch (desc)
	{
	case gps::llh_position::desc:
		{gps::llh_position::t tmp;
		if (p.size() < gps::llh_position::len-2) return;
		packet d(p.begin(), p.begin()+8); extract<double>(d, tmp.latitude);
		d.assign(p.begin()+8, p.begin()+16); extract<double>(d, tmp.longitude);
		d.assign(p.begin()+16, p.begin()+24); extract<double>(d, tmp.alt_ellipsoid);
		d.assign(p.begin()+24, p.begin()+32); extract<double>(d, tmp.alt_msl);
		d.assign(p.begin()+32, p.begin()+36); extract<float>(d, tmp.horizontal_acc);
		d.assign(p.begin()+36, p.begin()+40); extract<float>(d, tmp.vertical_acc);
		d.assign(p.begin()+40, p.begin()+42); extract<boost::uint16_t>(d, tmp.valid_flags);
		handler->llh_position(tmp);}
		break;
	case gps::ecef_position::desc:
		{gps::ecef_position::t tmp;
		if (p.size() < gps::ecef_position::len-2) return;
		packet d(p.begin(), p.begin()+8); extract<double>(d, tmp.x);
		d.assign(p.begin()+8, p.begin()+16); extract<double>(d, tmp.y);
		d.assign(p.begin()+16, p.begin()+24); extract<double>(d, tmp.z);
		d.assign(p.begin()+24, p.begin()+28); extract<float>(d, tmp.pos_acc);
		d.assign(p.begin()+28, p.begin()+30); extract<boost::uint16_t>(d, tmp.valid_flags);
		handler->ecef_position(tmp);}
		break;
	case gps::ned_velocity::desc:
		{gps::ned_velocity::t tmp;
		if (p.size() < gps::ned_velocity::len-2) return;
		packet d(p.begin(), p.begin()+4); extract<float>(d, tmp.north);
		d.assign(p.begin()+4, p.begin()+8); extract<float>(d, tmp.east);
		d.assign(p.begin()+8, p.begin()+12); extract<float>(d, tmp.down);
		d.assign(p.begin()+12, p.begin()+16); extract<float>(d, tmp.speed);
		d.assign(p.begin()+16, p.begin()+20); extract<float>(d, tmp.ground_spd);
		d.assign(p.begin()+20, p.begin()+24); extract<float>(d, tmp.heading);
		d.assign(p.begin()+24, p.begin()+28); extract<float>(d, tmp.spd_acc);
		d.assign(p.begin()+28, p.begin()+32); extract<float>(d, tmp.head_acc);
		d.assign(p.begin()+32, p.begin()+34); extract<boost::uint16_t>(d, tmp.valid_flags);
		handler->ned_velocity(tmp);}
		break;
	case gps::ecef_velocity::desc:
		{gps::ecef_velocity::t tmp;
		if (p.size() < gps::ecef_velocity::len-2) return;
		packet d(p.begin(), p.begin()+4); extract<float>(d, tmp.x);
		d.assign(p.begin()+4, p.begin()+8); extract<float>(d, tmp.y);
		d.assign(p.begin()+8, p.begin()+12); extract<float>(d, tmp.z);
		d.assign(p.begin()+12, p.begin()+16); extract<float>(d, tmp.vel_acc);
		d.assign(p.begin()+16, p.begin()+18); extract<boost::uint16_t>(d, tmp.valid_flags);
		handler->ecef_velocity(tmp);}
		break;
	case gps::dop_data::desc:
		{gps::dop_data::t tmp;
		if (p.size() < gps::dop_data::len-2) return;
		packet d(p.begin(), p.begin()+4); extract<float>(d, tmp.geom);
		d.assign(p.begin()+4, p.begin()+8); extract<float>(d, tmp.pos);
		d.assign(p.begin()+8, p.begin()+12); extract<float>(d, tmp.horiz);
		d.assign(p.begin()+12, p.begin()+16); extract<float>(d, tmp.vert);
		d.assign(p.begin()+16, p.begin()+20); extract<float>(d, tmp.time);
		d.assign(p.begin()+20, p.begin()+24); extract<float>(d, tmp.northing);
		d.assign(p.begin()+24, p.begin()+28); extract<float>(d, tmp.easting);
		d.assign(p.begin()+28, p.begin()+30); extract<boost::uint16_t>(d, tmp.valid_flags);
		handler->dop_data(tmp);}
		break;
	case gps::utc_time::desc:
		{gps::utc_time::t tmp;
		if (p.size() < gps::utc_time::len-2) return;
		packet d(p.begin(), p.begin()+2); extract<boost::uint16_t>(d, tmp.year);
		tmp.month = p[2]; tmp.day = p[3]; tmp.hour = p[4]; tmp.minute = p[5];
		tmp.second = p[6];
		d.assign(p.begin()+7, p.begin()+11); extract<boost::uint32_t>(d, tmp.millis);
		d.assign(p.begin()+11, p.begin()+13); extract<boost::uint16_t>(d, tmp.valid_flags);
		handler->utc_time(tmp);}
		break;
	case gps::gps_time::desc:
		{gps::gps_time::t tmp;
		if (p.size() < gps::gps_time::len-2) return;
		packet d(p.begin(), p.begin()+8); extract<double>(d, tmp.time_of_wk);
		d.assign(p.begin()+8, p.begin()+10); extract<boost::uint16_t>(d, tmp.wk_num);
		d.assign(p.begin()+10, p.begin()+12); extract<boost::uint16_t>(d, tmp.valid_flags);
		handler->gps_time(tmp);}
		break;
	case gps::clock_info::desc:
		{gps::clock_info::t tmp;
		if (p.size() < gps::clock_info::len-2) return;
		packet d(p.begin(), p.begin()+8); extract<double>(d, tmp.clk_bias);
		d.assign(p.begin()+8, p.begin()+16); extract<double>(d, tmp.clk_drift);
		d.assign(p.begin()+16, p.begin()+24); extract<double>(d, tmp.acc);
		d.assign(p.begin()+24, p.begin()+26); extract<boost::uint16_t>(d, tmp.valid_flags);
		handler->clock_info(tmp);}
		break;
	case gps::gps_fix::desc:
		{gps::gps_fix::t tmp;
		if (p.size() < gps::gps_fix::len-2) return;
		tmp.fix_type = p[0]; tmp.num_sats = p[1];
		packet d(p.begin()+2, p.begin()+4); extract<boost::uint16_t>(d, tmp.fix_flags);
		d.assign(p.begin()+4, p.begin()+6); extract<boost::uint16_t>(d, tmp.valid_flags);
		handler->gps_fix(tmp);}
		break;
	case gps::sat_info::desc:
		{gps::sat_info::t tmp;
		if (p.size() < gps::sat_info::len-2) return;
		tmp.channel = p[0]; tmp.id = p[1];
		packet d(p.begin()+2, p.begin()+4); extract<boost::uint16_t>(d, tmp.carrier_to_noise);
		d.assign(p.begin()+4, p.begin()+6); extract<boost::int16_t>(d, tmp.azimuth);
		d.assign(p.begin()+6, p.begin()+8); extract<boost::int16_t>(d, tmp.elevation);
		d.assign(p.begin()+8, p.begin()+10); extract<boost::uint16_t>(d, tmp.sat_flags);
		d.assign(p.begin()+10, p.begin()+12); extract<boost::uint16_t>(d, tmp.valid_flags);
		handler->sat_info(tmp);}
		break;
	case gps::gps_hdwr_status::desc:
		{gps::gps_hdwr_status::t tmp;
		if (p.size() < gps::gps_hdwr_status::len-2) return;
		tmp.sensor_state = p[0]; tmp.antenna_state = p[1]; tmp.antenna_pwr = p[2];
		packet d(p.begin()+3, p.begin()+5); extract<boost::uint16_t>(d, tmp.valid_flags);
		handler->gps_hdwr_status(tmp);}
		break;
	}
}

void GX3PacketHandler::parseCmd(packet &p, const char &desc)
{
	switch (desc)
	{
	case cmd::ack::desc:
		if (p.size() < cmd::ack::len-2) return;
		cmd::ack::t tmp; tmp.cmd = p[0]; tmp.err = p[1];
		acks.push_back(tmp);
		break;
	}
}
bool GX3PacketHandler::waitForAck(cmd::ack::t &a, unsigned int millis)
{
	a.cmd = a.err = 0;
	return true;
}

void GX3PacketHandler::setHandler(GX3Handler * h)
{
	handler = h;
}

void GX3PacketHandler::extract3floats(packet &p, float * f)
{
	packet tmp(p.begin(), p.begin()+4);
	extract<float>(tmp, f[0]);
	tmp.assign(p.begin()+4, p.begin()+8);
	extract<float>(tmp, f[1]);
	tmp.assign(p.begin()+8, p.begin()+12);
	extract<float>(tmp, f[2]);
}
void GX3PacketHandler::extractNfloats(packet &p, float *f, unsigned int n)
{
	if (p.size() < n*4)
		return;

	packet tmp(p.begin(), p.begin()+4);
	for (unsigned int cnt = 0; cnt < n; cnt++)
	{
		tmp.assign(p.begin()+cnt*4, p.begin()+(cnt+1)*4);
		extract<float>(tmp, f[cnt]);
	}
}

SerialGX335::SerialGX335(const std::string dev) : GX3PacketExtractor()
{
  
  uart = new UART(dev, 115200, 18);
  /*  
	char baudrate[] = {0x75,0x65,  0x0C,0x07,  0x07,0x40, 
			   0x01,//Apply
			   0x00, 0x01,0xC2,0x00,//Baudrate
			   0xFF,0xFF //Checksum
	};
       
	fletcher.appendChksm(baudrate, 13);
	if (!uart->write(baudrate, 13))
	  ROS_ERROR_STREAM("could not write to UART!");
  */
  
	//  delete uart;
	//  uart = new UART(dev, 230400, 18);
	gx3l = new GX3List(this);
	uart->addUARTList(gx3l);
}
SerialGX335::~SerialGX335()
{
	delete uart;
	delete gx3l;
}
bool SerialGX335::isGood()
{
	return uart->isOpen();
}
void SerialGX335::set_ahrs(const std::vector<char> &descs,
			const std::vector<char> &rateDecs)
{
	if (descs.size() != rateDecs.size())
		return;
	unsigned int len = 10 + 3*descs.size();
	std::vector<char> buff(len);
	buff[0] = 0x75; buff[1] = 0x65; buff[2] = 0x0C; buff[3] = 4+3*descs.size();
	buff[4] = buff[3]; buff[5] = 0x08; buff[6] = 0x01; buff[7] = descs.size();
	for (unsigned int x = 0; x<descs.size(); x++)
	{
		buff[8+3*x] = descs[x];
		buff[9+3*x] = (rateDecs[x] >> 8) & 0xFF;
		buff[10+3*x] = rateDecs[x] & 0xFF;
	}
	fletcher.appendChksm(buff);
	_set_ahrs(buff);
}
void SerialGX335::set_ahrs(const std::vector<char> &descs,
			unsigned short rateDec)
{
	unsigned int len = 10 + 3*descs.size();
	std::vector<char> buff(len);
	buff[0] = 0x75; buff[1] = 0x65; buff[2] = 0x0C; buff[3] = 4+3*descs.size();
	buff[4] = buff[3]; buff[5] = 0x08; buff[6] = 0x01; buff[7] = descs.size();
	for (unsigned int x = 0; x<descs.size(); x++)
	{
		buff[8+3*x] = descs[x];
		buff[9+3*x] = (rateDec >> 8) & 0xFF;
		buff[10+3*x] = rateDec & 0xFF;
	}
	fletcher.appendChksm(buff);
	_set_ahrs(buff);
}
void SerialGX335::set_gyro_bias(const std::vector<float> &descs)
{
	unsigned int len = 9 + 4*descs.size();
	std::vector<char> buff(len);
	buff[0] = 0x75; buff[1] = 0x65; buff[2] = 0x0C; buff[3] = 0x0F;
	buff[4] = buff[3]; buff[5] = 0x38; buff[6] = 0x01; 
	for (unsigned int x = 0; x<descs.size(); x++)
	{
		unsigned char *d=(unsigned char *) &descs[x];
		buff[7+4*x]=(*d >> 24) &0xFF;
		buff[8+4*x]=(*d >> 16) &0xFF;
		buff[9+4*x]=(*d >> 8) &0xFF;
		buff[10+4*x]=(*d) &0xFF;
	}
	fletcher.appendChksm(buff);
	for (unsigned int x = 0;x<len;x++){
		cout<<buff[x]<<", ";
	
	}
	cout<<endl;
	//_set_ahrs(buff);
}
void SerialGX335::_set_ahrs(const packet &config_pkt)
{
	char stop[] = { 0x75, 0x65, 0x0C, 0x05,
					0x05, 0x11, 0x01, 0x01,
					0x00, 0xFF, 0xFF };
	fletcher.appendChksm(stop, 11);
	if (!uart->write(stop, 11))
	  ROS_ERROR_STREAM("could not write to UART!");

	if (!uart->write(config_pkt))
	  ROS_ERROR_STREAM("could not write to UART!");	
	//\HACK set disable magnetometer and other settings:

	char signalconditioning[] = {0x75,0x65,  0x0C,0x10,  0x10,0x35, 
				     0x01,//Apply
				     0x00, 0x0A,//Calc Decimation
				     //0x14, 0x03,//Flags no north correction
				     0x10, 0x03,//Flags with north correction
				     0x0E,//Acc gyro filtering
				     0x11,//Magnetometer filtering
				     0x00, 0x0A,//Up compensation
				     0x00, 0x0A,//North compensation
				     0x00,//Mag BW
				     0x00, 0x00,//Reserved
				     0xFF,0xFF //Checksum
	};
       
	fletcher.appendChksm(signalconditioning, 22);
	if (!uart->write(signalconditioning, 22))
	  ROS_ERROR_STREAM("could not write to UART!");

//to increase bandwidth to get higher frequency output from imu
/*	char baudrate[]={0x75, 0x65, 0x0C, 0x07 , 0x07, 0x40 , 0x01, 0x00 , 0x1C, 0x20 , 0x00, 0xF8 , 0xDA};
	if (!uart->write(baudrate, 13))
	  ROS_ERROR_STREAM("could not write to UART!");*/

	char save[] = { 0x75, 0x65, 0x0C, 0x04,
					0x04, 0x08, 0x03, 0x00,
					0xFF, 0xFF };
	fletcher.appendChksm(save, 10);
	if (!uart->write(save, 10))
	       	  ROS_ERROR_STREAM("could not write to UART!");

	char start[] = { 0x75, 0x65, 0x0C, 0x05,
					0x05, 0x11, 0x01, 0x01,
					0x01, 0xFF, 0xFF };
	fletcher.appendChksm(start, 11);
	if (!uart->write(start, 11))
	  ROS_ERROR_STREAM("could not write to UART!");
	     
}
void SerialGX335::set_gps(const std::vector<char> &descs,
			const std::vector<char> &rateDecs)
{
	if (descs.size() != rateDecs.size())
		return;
	unsigned int len = 10 + 3*descs.size();
	std::vector<char> buff(len);
	buff[0] = 0x75; buff[1] = 0x65; buff[2] = 0x0C; buff[3] = 4+3*descs.size();
	buff[4] = buff[3]; buff[5] = 0x09; buff[6] = 0x01; buff[7] = descs.size();
	for (unsigned int x = 0; x<descs.size(); x++)
	{
		buff[8+3*x] = descs[x];
		buff[9+3*x] = (rateDecs[x] >> 8) & 0xFF;
		buff[10+3*x] = rateDecs[x] & 0xFF;
	}
	fletcher.appendChksm(buff);
	_set_gps(buff);
}
void SerialGX335::set_gps(const std::vector<char> &descs,
			unsigned short rateDec)
{
	unsigned int len = 10 + 3*descs.size();
	std::vector<char> buff(len);
	buff[0] = 0x75; buff[1] = 0x65; buff[2] = 0x0C; buff[3] = 4+3*descs.size();
	buff[4] = buff[3]; buff[5] = 0x09; buff[6] = 0x01; buff[7] = descs.size();
	for (unsigned int x = 0; x<descs.size(); x++)
	{
		buff[8+3*x] = descs[x];
		buff[9+3*x] = (rateDec >> 8) & 0xFF;
		buff[10+3*x] = rateDec & 0xFF;
	}
	fletcher.appendChksm(buff);
	_set_gps(buff);
}
void SerialGX335::_set_gps(const packet &config_pkt)
{
	char stop[] = { 0x75, 0x65, 0x0C, 0x05,
					0x05, 0x11, 0x01, 0x02,
					0x00, 0xFF, 0xFF };
	fletcher.appendChksm(stop, 11);
	if (!uart->write(stop, 11))
	  ROS_ERROR_STREAM("could not write to UART!");
	     



	if (!uart->write(config_pkt))
	  ROS_ERROR_STREAM("could not write to UART!");	
	

	//\HACK set the gps mode to be airborne

	/*	char gpsmode[] = {0x75,0x65,  0x0C,0x04,  0x04,0x34, 
				     0x01,//Apply
				     0x08,// GPS Mode
				     0xFF,0xFF //Checksum
	};
       
	fletcher.appendChksm(gpsmode, 10);
	if (!uart->write(gpsmode, 10))
	  ROS_ERROR_STREAM("could not write to UART!");
	*/    



	char save[] = { 0x75, 0x65, 0x0C, 0x04,
					0x04, 0x09, 0x03, 0x00,
					0xFF, 0xFF };
	fletcher.appendChksm(save, 10);
	if (!uart->write(save, 10))
	  ROS_ERROR_STREAM("could not write to UART!");	
	
	char start[] = { 0x75, 0x65, 0x0C, 0x05,
					0x05, 0x11, 0x01, 0x02,
					0x01, 0xFF, 0xFF };
	fletcher.appendChksm(start, 11);
	if (!uart->write(start, 11))
	  ROS_ERROR_STREAM("could not write to UART!");
	 
}

unsigned int Fletcher16::length() { return 2; }
bool Fletcher16::check(const packet &p)
{
	char s1=0,s2=0;
	for (unsigned int i=0; i<p.size()-2; i++)
	{
		s1 = (s1+p[i])%256;
		s2 = (s2+s1)%256;
	}
	bool result = (p[p.size()-2] == s1 && p[p.size()-1] == s2);
	if (!result)
	  ROS_WARN_STREAM("BAD CHECKSUM!");
	return result;
}
void Fletcher16::appendChksm(packet &p)
{
	char s1=0,s2=0;
	for (unsigned int i=0; i<p.size()-2; i++)
	{
		s1 = (s1+p[i])%256;
		s2 = (s2+s1)%256;
	}
	p[p.size()-2] = s1;
	p[p.size()-1] = s2;
}

void Fletcher16::appendChksm(char *p, unsigned int len)
{
	char s1=0,s2=0;
	for (unsigned int i=0; i<len-2; i++)
	{
		s1 = (s1+p[i])%256;
		s2 = (s2+s1)%256;
	}
	p[len-2] = s1;
	p[len-1] = s2;
}
