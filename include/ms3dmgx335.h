/*
 * Copyright 2011 Virginia Polytechnic Institute and State University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 		* Redistributions of source code must retain the above copyright
 * 		  notice, this list of conditions and the following disclaimer.
 * 		* Redistributions in binary form must reproduce the above copyright
 * 		  notice, this list of conditions and the following disclaimer in the
 * 		  documentation and/or other materials provided with the distribution.
 * 		* Neither the name of the <organization> nor the
 * 		  names of its contributors may be used to endorse or promote products
 * 		  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * ms3dmgx335.h
 *
 * This contains the interfaces for each class involved with 3DM-GX3-35 parsing.
 *
 * GX3Handler - the user implements this, user functions are automatically
 * 		called from a parallel thread when new data is received.  This is the
 * 		same thread for the "read" function, so any blocking will prevent any
 * 		future reads.
 * GX3PacketHandler - performs the "meat" of any parsing.  Use the "setHandler"
 * 		function to change the GX3Handler that is used.
 * Fletcher16 - calculates the checksums for the parser, and appends checksums
 * 		for writing data
 * GX3PacketExtractor - inherits from PacketExtractor, and automatically sets
 * 		up a GX3PacketHandler to handle packets (feeds through the "setHandler"
 * 		function)
 * SerialGX335 - inherits from GX3PacketExtractor, and incorporates the UART
 * 		class, along with setting both AHRS and GPS packets.
 *
 *
 *  Created on: April 13, 2011
 *      Author: Kevin Stefanik (stefanik08@gmail.com)
 */

#ifndef MS3DMGX335_H_
#define MS3DMGX335_H_

#include "uart.h"
#include "parser.h"
#include "ms3dmgx335-structs.h"
#include <vector>

namespace microstrain
{
namespace gx335
{

typedef PacketExtractor::packet packet;

//swap must be performed to convert big endian to little endian
#define SWAP2(x,y) {x=x^y;y=x^y;x=x^y;}
template<typename T>
bool extract(packet &p, T &d)
{
	if (p.size() < sizeof(T))
	{
		return false;
	}
	switch (sizeof(T))
	{
	case 1:
		d = (T)p[0];
		break;
	case 2:
		SWAP2(p[0], p[1])
		break;
	case 4:
		SWAP2(p[0], p[3])
		SWAP2(p[1], p[2])
		break;
	case 8:
		SWAP2(p[0], p[7])
		SWAP2(p[1], p[6])
		SWAP2(p[2], p[5])
		SWAP2(p[3], p[4])
		break;
	}
	char * ptr = (char*)&d;
	for (unsigned int i=0; i < sizeof(T); i++)
		ptr[i] = p[i];
	return true;
}

class GX3Handler
{
public:
	virtual void raw_accel(const ahrs::raw_accel::t &d) {}
	virtual void raw_gyro(const ahrs::raw_gyro::t &d) {}
	virtual void raw_mag(const ahrs::raw_mag::t &d) {}
	virtual void scaled_accel(const ahrs::scaled_accel::t &d) {}
	virtual void scaled_gyro(const ahrs::scaled_gyro::t &d) {}
	virtual void scaled_mag(const ahrs::scaled_mag::t &d) {}
	virtual void delta_theta(const ahrs::delta_theta::t &d) {}
	virtual void delta_velocity(const ahrs::delta_velocity::t &d) {}
	virtual void orien_mat(const ahrs::orien_mat::t &d) {}
	virtual void quaternion(const ahrs::quaternion::t &d) {}
	virtual void orien_update(const ahrs::orien_update::t &d) {}
	virtual void euler_angles(const ahrs::euler_angles::t &d) {}
	virtual void internal_time(const ahrs::internal_time::t &d) {}
	virtual void beacon_time(const ahrs::beacon_time::t &d) {}
	virtual void stab_mag(const ahrs::stab_mag::t &d) {}
	virtual void stab_accel(const ahrs::stab_accel::t &d) {}
	virtual void gps_time(const ahrs::gps_time::t &d) {}

	virtual void llh_position(const gps::llh_position::t &d) {}
	virtual void ecef_position(const gps::ecef_position::t &d) {}
	virtual void ned_velocity(const gps::ned_velocity::t &d) {}
	virtual void ecef_velocity(const gps::ecef_velocity::t &d) {}
	virtual void dop_data(const gps::dop_data::t &d) {}
	virtual void utc_time(const gps::utc_time::t &d) {}
	virtual void gps_time(const gps::gps_time::t &d) {}
	virtual void clock_info(const gps::clock_info::t &d) {}
	virtual void gps_fix(const gps::gps_fix::t &d) {}
	virtual void sat_info(const gps::sat_info::t &d) {}
	virtual void gps_hdwr_status(const gps::gps_hdwr_status::t &d) {}

	virtual void eop() {} //called at the end of a data packet
};

class GX3PacketHandler : public PacketExtractor::PacketHandler
{
public:
	GX3PacketHandler(GX3Handler * h=0);

	virtual void handle(const packet &p);
	void parseAhrs(packet &p, const char &desc);
	void parseGps(packet &p, const char &desc);
	void parseCmd(packet &p, const char &desc);

	void setHandler(GX3Handler * h);

	bool waitForAck(cmd::ack::t &a, unsigned int millis);
private:
	void extract3floats(packet &p, float * f);
	void extractNfloats(packet &p, float *f, unsigned int n);
	GX3Handler * handler;
	std::list<cmd::ack::t> acks;
};

class Fletcher16 : public PacketExtractor::Checksum
{
public:
	unsigned int length();
	bool check(const packet &p);
	void appendChksm(packet &p);
	void appendChksm(char *p, unsigned int len);
};

class GX3PacketExtractor : private PacketExtractor
{
public:
	GX3PacketExtractor() : PacketExtractor(4,3,sync_pkt,&GX3PacketExtractor::fletcher)
	{
		this->addHandler(&gx3h);
	}
	void setGX3Handler(GX3Handler * h)
	{
		gx3h.setHandler(h);
	}
private:
	GX3PacketHandler gx3h;
	static Fletcher16 fletcher;
	static const char sync_data[2];
	static const packet sync_pkt;
	friend class SerialGX335;
};

class SerialGX335 : public GX3PacketExtractor
{
public:
  SerialGX335(const std::string dev);
	~SerialGX335();
	bool isGood();
	//ahrs frequency = 1000Hz/rateDec
	void set_ahrs(const std::vector<char> &descs,
			const std::vector<char> &rateDecs);
	void set_ahrs(const std::vector<char> &descs,
				unsigned short rateDec);
	void set_gyro_bias(const std::vector<float> &descs);

	//gps frequency = 4Hz/rateDec
	void set_gps(const std::vector<char> &descs,
			const std::vector<char> &rateDecs);
	void set_gps(const std::vector<char> &descs,
				unsigned short rateDec);

	void close()
	{
		uart->close();
	}
private:
	void _set_ahrs(const packet &config_pkt);
	void _set_gps(const packet &config_pkt);
	UART * uart;
	class GX3List : public UART::UARTListener
	{
	public:
		GX3List(PacketExtractor* p)
		{
			pex = p;
		}
		/**
		 * process(char*, int) - This function is called every time up to
		 * 		pcktSize (specified in constructor) bytes are read.  Note that
		 * 		pcktSize is not settable to avoid synchronous issues.
		 * @param data is the byte array to process
		 * @param len is the length of the byte array
		 */
		virtual void process(const char* data, int len)
		{
			pex->addData(data, len);
		}
	private:
		PacketExtractor* pex;
	};
	GX3List * gx3l;
};

}
}

#endif /* MS3DMGX335_H_ */
