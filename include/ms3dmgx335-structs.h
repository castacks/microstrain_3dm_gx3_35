/*
 * Copyright 2011 Virginia Polytechnic Institute and State University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 		* Redistributions of source code must retain the above copyright
 * 		  notice, this list of conditions and the following disclaimer.
 * 		* Redistributions in binary form must reproduce the above copyright
 * 		  notice, this list of conditions and the following disclaimer in the
 * 		  documentation and/or other materials provided with the distribution.
 * 		* Neither the name of the <organization> nor the
 * 		  names of its contributors may be used to endorse or promote products
 * 		  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * ms3dmgx335-structs.h
 *
 * This file contains all the Microstrain 3DM-GX3-35 communication protocol
 * information, including datatypes, most of which are represented as a struct.
 *
 * Note that each datatype actually has its own namespace which includes "desc"
 * which is the descriptor byte in the raw message, "len" which is the byte
 * length of the raw data, and "t" which is the type of the data (struct,
 * int, etc...)
 *
 *  Created on: Apr 13, 2011
 *      Author: Kevin Stefanik (stefanik08@gmail.com)
 */

#ifndef MS3DMGX335_STRUCTS_H_
#define MS3DMGX335_STRUCTS_H_

#include <vector>
#include <boost/cstdint.hpp>

namespace microstrain
{
namespace gx335
{
const char sync_head[] = {0x75, 0x65};
const unsigned int desc_ind = 2;

namespace cmd
{

const char desc = 0x0C;

namespace ack
{
const char len = 4, desc = 0xF1;
typedef struct {
	char cmd;
	char err;
} t;
}

} //cmd

namespace ahrs
{

typedef struct {
	float x;
	float y;
	float z;
} _xyzfloats;

const char desc = 0x80;

namespace raw_accel
{
const char len = 14, desc = 0x01;
typedef struct {
	float accel1;
	float accel2;
	float accel3;
} t;
}

namespace raw_gyro
{
const char len = 14, desc = 0x02;
typedef struct {
	float gyro1;
	float gyro2;
	float gyro3;
} t;
}

namespace raw_mag
{
const char len = 14, desc = 0x03;
typedef struct {
	float mag1;
	float mag2;
	float mag3;
} t;
}

namespace scaled_accel
{
const char len = 14, desc = 0x04;
typedef _xyzfloats t;
}

namespace scaled_gyro
{
const char len = 14, desc = 0x05;
typedef _xyzfloats t;
}

namespace scaled_mag
{
const char len = 14, desc = 0x06;
typedef _xyzfloats t;
}

namespace delta_theta
{
const char len = 14, desc = 0x07;
typedef _xyzfloats t;
}

namespace delta_velocity
{
const char len = 14, desc = 0x08;
typedef _xyzfloats t;
}

namespace orien_mat
{
const char len = 38, desc = 0x09;
typedef struct {
	float M[9];
}  t;
//short-hand for indexing an orien_mat.t, i=row, j=col
#define ACCESS_ORIEN(om, i, j) (om.M[3*i+j])
}

namespace quaternion
{
const char len = 18, desc = 0x0A;
typedef struct {
	float q[4];
}  t;
}

namespace orien_update
{
const char len = 38, desc = 0x0B;
typedef struct {
	float C[9];
}  t;
}

namespace euler_angles
{
const char len = 14, desc = 0x0C;
typedef struct {
	float roll;
	float pitch;
	float yaw;
}  t;
}

namespace internal_time
{
const char len = 6, desc = 0x0E;
typedef unsigned int t;
}

namespace beacon_time
{
const char len = 11, desc = 0x0F;
typedef struct {
	unsigned char status;
	unsigned int pps_cnt;
	unsigned int ns_cnt;
}  t;
}

namespace stab_mag
{
const char len = 14, desc = 0x10;
typedef _xyzfloats t;
}

namespace stab_accel
{
const char len = 14, desc = 0x11;
typedef _xyzfloats t;
}

namespace gps_time
{
const char len = 14, desc = 0x12;
typedef struct {
	double time_of_wk;
	boost::uint16_t wk_num;
	boost::uint16_t valid_flags;
} t;
}

} //ahrs

namespace gps
{
const char desc = 0x81;

namespace llh_position
{
const char len = 44, desc = 0x03;
typedef struct {
	double latitude;
	double longitude;
	double alt_ellipsoid;
	double alt_msl;
	float horizontal_acc;
	float vertical_acc;
	boost::uint16_t valid_flags;
} t;
}

namespace ecef_position
{
const char len = 32, desc = 0x04;
typedef struct {
	double x;
	double y;
	double z;
	float pos_acc;
	boost::uint16_t valid_flags;
} t;
}

namespace ned_velocity
{
const char len = 36, desc = 0x05;
typedef struct {
	float north;
	float east;
	float down;
	float speed;
	float ground_spd;
	float heading;
	float spd_acc;
	float head_acc;
	boost::uint16_t valid_flags;
} t;
}

namespace ecef_velocity
{
const char len = 20, desc = 0x06;
typedef struct {
	float x;
	float y;
	float z;
	float vel_acc;
	boost::uint16_t valid_flags;
} t;
}

namespace dop_data
{
const char len = 30, desc = 0x07;
typedef struct {
	float geom;
	float pos;
	float horiz;
	float vert;
	float time;
	float northing;
	float easting;
	boost::uint16_t valid_flags;
} t;
}

namespace utc_time
{
const char len = 15, desc = 0x08;
typedef struct {
	boost::uint16_t year;
	boost::uint8_t month;
	boost::uint8_t day;
	boost::uint8_t hour;
	boost::uint8_t minute;
	boost::uint8_t second;
	boost::uint32_t millis;
	boost::uint16_t valid_flags;
} t;
}

namespace gps_time
{
const char len = 14, desc = 0x09;
typedef struct {
	double time_of_wk;
	boost::uint16_t wk_num;
	boost::uint16_t valid_flags;
} t;
}

namespace clock_info
{
const char len = 28, desc = 0x0A;
typedef struct {
	double clk_bias;
	double clk_drift;
	double acc;
	boost::uint16_t valid_flags;
} t;
}

namespace gps_fix
{
const char len = 8, desc = 0x0B;
typedef struct {
	boost::uint8_t fix_type;
	boost::uint8_t num_sats;
	boost::uint16_t fix_flags;
	boost::uint16_t valid_flags;
} t;
}

namespace sat_info
{
const char len = 14, desc = 0x0C;
typedef struct {
	boost::uint8_t channel;
	boost::uint8_t id;
	boost::uint16_t carrier_to_noise;
	boost::int16_t azimuth;
	boost::int16_t elevation;
	boost::uint16_t sat_flags;
	boost::uint16_t valid_flags;
} t;
}

namespace gps_hdwr_status
{
const char len = 7, desc = 0x0D;
typedef struct {
	boost::uint8_t sensor_state;
	boost::uint8_t antenna_state;
	boost::uint8_t antenna_pwr;
	boost::uint16_t valid_flags;
} t;
}
//ubx, nmea, not implemented:
namespace raw_nmea
{
const char len = -1, desc = 0x01;
typedef std::vector<char> t;
}

namespace raw_ubx
{
const char len = -1, desc = 0x02;
typedef std::vector<char> t;
}


} //gps

} //gx335
} //microstrain


#endif /* MS3DMGX335_STRUCTS_H_ */
