/*
 * Copyright 2011 Virginia Polytechnic Institute and State University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 		* Redistributions of source code must retain the above copyright
 * 		  notice, this list of conditions and the following disclaimer.
 * 		* Redistributions in binary form must reproduce the above copyright
 * 		  notice, this list of conditions and the following disclaimer in the
 * 		  documentation and/or other materials provided with the distribution.
 * 		* Neither the name of the <organization> nor the
 * 		  names of its contributors may be used to endorse or promote products
 * 		  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *
 * parser.h
 *
 * This file defines a PacketExtractor.  A packet requires a sync packet, a
 * header length, an index within the header to find packet length, and an
 * optional checksum. It will then send the packet data to a user-defined
 * PacketHandler (a subclass of PacketExtractor).
 *
 *  Created on: April 13, 2011
 *      Author: Kevin Stefanik (stefanik08@gmail.com)
 */

#ifndef PARSER_H_
#define PARSER_H_

#include <vector>
#include <list>

class PacketExtractor
{
public:
	typedef std::vector<char> packet;
	class PacketHandler
	{
	public:
		virtual void handle(const packet &p) = 0;
	};

	class Checksum
	{
	public:
		virtual unsigned int length()
		{
			return 0;
		}
		virtual bool check(const packet &p)
		{
			return true;
		}
	};

	PacketExtractor(unsigned int hdr_len, unsigned int pkt_len_ind,
			const packet &s = packet(), Checksum * c = 0);
	void addHandler(PacketHandler * h);
	void removeHandler(PacketHandler * h);

	//the addData commands extract a packet from a stream that begins with
	//sync,
	bool addData(const char * data, unsigned int len);
	bool addData(char data);

private:
	std::list<PacketHandler*> hs;
	packet sync;

	//storage for just the header:
	packet hdr_storage;
	unsigned int hdr_ind;

	//storage for the whole packet (changes size with given packet)
	packet pkt_storage;
	unsigned int pkt_ind;

	unsigned int len_ind;

	Checksum * chksm;
};

#endif /* PARSER_H_ */
